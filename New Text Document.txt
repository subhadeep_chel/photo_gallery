<!DOCTYPE html>
<html>
<head>
<style>
div.gallery {
  border: 1px solid #ccc;
}

div.gallery:hover {
  border: 1px solid #777;
}

div.gallery img {
  width: 100%;
  height: auto;
}

div.desc {
  padding: 15px;
  text-align: center;
}

* {
  box-sizing: border-box;
}

.responsive {
  padding: 0 6px;
  float: left;
  width: 24.99999%;
}

@media only screen and (max-width: 700px) {
  .responsive {
    width: 49.99999%;
    margin: 6px 0;
  }
}

@media only screen and (max-width: 500px) {
  .responsive {
    width: 100%;
  }
}

.clearfix:after {
  content: "";
  display: table;
  clear: both;
}
</style>
</head>
<body>

<h2>Responsive Image Gallery</h2>
<h4>Resize the browser window to see the effect.</h4>

<div class="responsive">
  <div class="gallery">
    <a target="_blank" href="img_5terre.jpg">
      <img src="IMG_20180930_201746.jpg" alt="Cinque Terre" width="600" height="400">
    </a>
    <div class="desc">Ganges River</div>
  </div>
</div>


<div class="responsive">
  <div class="gallery">
    <a target="_blank" href="img_forest.jpg">
      <img src="IMG_20181002_164151.jpg" alt="Forest" width="600" height="400">
    </a>
    <div class="desc">Second Hoogly Bridge</div>
  </div>
</div>

<div class="responsive">
  <div class="gallery">
    <a target="_blank" href="img_lights.jpg">
      <img src="IMG_20181007_223607.jpg" alt="Northern Lights" width="600" height="400">
    </a>
    <div class="desc">St. Paul's Cathedral</div>
  </div>
</div>

<div class="responsive">
  <div class="gallery">
    <a target="_blank" href="img_mountains.jpg">
      <img src="PSX_20180710_210814.jpg" alt="Mountains" width="600" height="400">
    </a>
    <div class="desc">Gateway Of Kolkata</div>
  </div>
</div>

<div class="clearfix"></div>

<div style="padding:6px;">
  <p>Kolkata</p>
</div>

</body>
</html>
